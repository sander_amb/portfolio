<?php
/**
 * Template Name: Portfolio Overview
 */
$context = Timber::get_context();
$context['projects'] = Timber::get_posts(array(
	'post_type' => 'portfolio',
));
$context['post'] = new TimberPost();

Timber::render('page-templates/pt-portfolio.twig', $context);