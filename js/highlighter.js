(function($){
	hljs.configure({
		'tabReplace': '    '
	});
	hljs.initHighlighting();
})(jQuery);