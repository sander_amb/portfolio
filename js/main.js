(function($){
	var  $doc = $(document);
	$.readyFn = {
		list: [],
		register: function(fn) {
			$.readyFn.list.push(fn);
		},
		execute: function() {
			for (var i = 0; i < $.readyFn.list.length; i++) {
				try {
				   $.readyFn.list[i].apply(document, [$]);
				}
				catch (e) {
					throw e;
				}
			};
		}
	};

	var application = {
		initSmooth: function() {
			var $body = $('html, body');
			var smoothState = $('#main').smoothState({
				onStart: {
					duration: 250,
					render: function($container){
						$container.addClass('is-exiting');

						$body.animate({scrollTop: 0});

						smoothState.restartCSSAnimations();
					}
				},
				onReady: {
					duration: 0,
					render: function($container, $newContainer) {
						$container.removeClass('is-exiting');
						$container.html($newContainer);
					}
				},
				onAfter: function($container, $newContent) {
					$.readyFn.execute();
				}
			}).data('smoothState');
		},
		initFuncs: function() {
			var $body = $('body');

			if(typeof hljs !== 'undefined')
			{
				hljs.configure({
					'tabReplace': '    '
				});
				$('code').each(function(i, block) {
					hljs.highlightBlock(block);
				});
			}

			$togglr = $('.navigation-toggle');
			$togglr.on('click', function(e){
				e.preventDefault();
				$body.toggleClass('menu-active');
			});
		}
	}

	$doc.ready(function(){
		application.initSmooth();
		$.readyFn.execute();
	});

	$.fn.ready = function(fn) {
		$.readyFn.register(fn);
	};

	$(function(){ application.initFuncs(); })
})(jQuery);