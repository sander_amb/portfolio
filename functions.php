<?php
/**
 * @package AmbientBit
 */

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="'.esc_url(admin_url('plugins.php#timber')).'">'.esc_url(admin_url('plugins.php')).'</a></p></div>';
		});
	return;
}

Timber::$dirname = array('templates', 'views');

class PortfolioSite extends TimberSite {

	function __construct()
	{
		add_theme_support('post-thumbnails');
		add_theme_support('menus');

		add_filter('timber_context', array($this, 'add_to_context'));
		add_filter('get_twig', array($this, 'add_to_twig'));
		add_action('init', array($this, 'register_post_types'));
		add_action('init', array($this, 'register_taxonomies'));
		add_action('init', array($this, 'register_menus'));

		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));

		parent::__construct();
	}

	function register_post_types()
	{
		$pf_singular = 'Portfolio item';
		$pf_plural = 'Portfolio items';
		$pf_labels = array(
			'name' => $pf_plural,
			'singular_name' => $pf_singular,
			'add_new' => 'Add New',
			'add_new_item' => 'Add New ' . $pf_singular,
			'edit_item' => 'Edit ' . $pf_singular,
			'new_item' => 'New ' . $pf_singular,
			'view_item' => 'View ' . $pf_singular,
			'search_items' => 'Search ' . $pf_plural,
			'not_found' => 'No ' . strtolower($pf_plural) . ' found',
			'not_found_in_trash' => 'No ' . strtolower($pf_plural) . ' found in Trash',
			'parent_item_colon' => 'Parent ' . $pf_singular,
			'all_items' => 'All ' . $pf_plural,
			'archives' => $pf_singular . ' Archives',
			'insert_into_item' => 'Insert into ' . strtolower($pf_singular),
			'uploaded_to_this_item' => 'Uploaded to this ' . strtolower($pf_singular)
		);
		$pf_args = array(
			'labels' => $pf_labels,
			'description' => 'Our ' . $pf_plural,
			'public' => true,
			'show_in_nav_menus' => false,
			'menu_position' => null,
			'capability_type' => 'post',
			'supports' => array('title', 'editor', 'thumbnail'),
			'has_archive' => false,
			'rewrite' => true,
			'can_export' => true,
			'show_in_rest' => true,
			'menu_icon' => 'dashicons-portfolio',
		);
		register_post_type('portfolio', $pf_args);
	}

	function register_taxonomies()
	{
	}

	function add_to_context($context)
	{
		$context['menu'] = new TimberMenu('primary_menu');
		$context['site'] = $this;
		$context['default_keyvisual'] = new TimberImage(get_template_directory_uri() . '/img/key_blog.png');
		return $context;
	}

	function add_to_twig($twig)
	{
		$twig->addExtension(new Twig_Extension_StringLoader());
		return $twig;
	}

	function enqueue_scripts()
	{
		// enqueue styles
		wp_enqueue_style('hljs', get_template_directory_uri(). '/css/agate.css', array(), '1.0.0');
		wp_enqueue_style('theme', get_template_directory_uri() . '/css/style.css', array(), '1.0.0');

		// enqueue scripts
		if(!is_admin())
		{
			wp_deregister_script('jquery');
			wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js', array(), '1.12.2');
		}
		wp_enqueue_script('smoothstate', get_template_directory_uri().'/js/jquery.smoothState.js', array('jquery'));
		wp_enqueue_script('hljs', get_template_directory_uri().'/js/highlight.pack.js', array('jquery', 'smoothstate'));

		wp_enqueue_script('smooth_handler', get_template_directory_uri().'/js/main.js', array('jquery', 'smoothstate', 'hljs'), '1.0.0', true);
		
	}
	function register_menus()
	{
		register_nav_menus(array(
			'primary_menu' => __('Primary Menu', 'sb_portfolio')
		));
	}
}
new PortfolioSite();

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_keyvisual',
		'title' => 'Keyvisual',
		'fields' => array (
			array (
				'key' => 'field_575ed0770c176',
				'label' => 'Size',
				'name' => 'size',
				'type' => 'radio',
				'required' => 1,
				'choices' => array (
					'Small' => 'Small',
					'Large' => 'Large',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_575ed02e0c174',
				'label' => 'Show label',
				'name' => 'show_label',
				'type' => 'radio',
				'choices' => array (
					'Yes' => 'Yes',
					'No' => 'No',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_575ed0580c175',
				'label' => 'Label',
				'name' => 'label',
				'type' => 'text',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_575ed02e0c174',
							'operator' => '==',
							'value' => 'Yes',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				)
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_project-information',
		'title' => 'Project Information',
		'fields' => array (
			array (
				'key' => 'field_575fff62ffefe',
				'label' => 'Creation date',
				'name' => 'project_date',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'dd/mm/yy',
				'first_day' => 1,
			),
			array (
				'key' => 'field_575fff80ffeff',
				'label' => 'Created with',
				'name' => 'project_created',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'portfolio',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
